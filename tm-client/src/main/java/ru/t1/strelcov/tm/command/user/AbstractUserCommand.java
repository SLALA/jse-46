package ru.t1.strelcov.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.command.AbstractCommand;
import ru.t1.strelcov.tm.dto.model.UserDTO;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(@Nullable final UserDTO user) {
        if (user == null) return;
        System.out.println("[Id]: " + user.getId());
        System.out.println("[Login]: " + user.getLogin());
        System.out.println("[Email]: " + user.getEmail());
        System.out.println("[First Name]: " + user.getFirstName());
        System.out.println("[Last Name]: " + user.getLastName());
    }

}
