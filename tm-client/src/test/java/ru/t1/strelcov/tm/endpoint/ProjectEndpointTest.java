package ru.t1.strelcov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.strelcov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.strelcov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.ProjectListResponse;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.marker.IntegrationCategory;
import ru.t1.strelcov.tm.service.PropertyService;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String host = propertyService.getServerHost();

    @NotNull
    private final Integer port = propertyService.getServerPort();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(host, port);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @Nullable
    private String token;

    @Before
    public void before() {
        token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        projectEndpoint.createProject(new ProjectCreateRequest(token, "pr2", "pr2"));
        projectEndpoint.createProject(new ProjectCreateRequest(token, "pr1", "pr1"));
    }

    @After
    public void after() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        authEndpoint.logout(new UserLogoutRequest());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void listProjectsTest() {
        Assert.assertEquals(2, projectEndpoint.listProject(new ProjectListRequest(token)).getList().size());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void listSortedProjectsTest() {
        @NotNull final List<ProjectDTO> list = projectEndpoint.listSortedProject(new ProjectListSortedRequest(token, SortType.NAME.name())).getList();
        Assert.assertEquals(2, list.size());
        @NotNull final Comparator<ProjectDTO> comparator = SortType.NAME.getComparator();
        Assert.assertEquals(list, list.stream().sorted(comparator).collect(Collectors.toList()));
    }

    @Category(IntegrationCategory.class)
    @Test
    public void createProjectTest() {
        int size = projectEndpoint.listProject(new ProjectListRequest(token)).getList().size();
        projectEndpoint.createProject(new ProjectCreateRequest(token, "pr2", "pr2"));
        Assert.assertEquals(size + 1, projectEndpoint.listProject(new ProjectListRequest(token)).getList().size());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void clearProjectTest() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        ProjectListResponse response = projectEndpoint.listProject(new ProjectListRequest(token));
        Assert.assertEquals(0, projectEndpoint.listProject(new ProjectListRequest(token)).getList().size());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void findByIdProjectTest() {
        @NotNull final ProjectDTO expectedProject = projectEndpoint.listProject(new ProjectListRequest(token)).getList().get(0);
        @NotNull final ProjectDTO actualProject = projectEndpoint.findByIdProject(new ProjectFindByIdRequest(token, expectedProject.getId())).getProject();
        Assert.assertEquals(expectedProject.getId(), actualProject.getId());
        Assert.assertEquals(expectedProject.getName(), actualProject.getName());
        Assert.assertEquals(expectedProject.getCreated(), actualProject.getCreated());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void findByNameProjectTest() {
        @NotNull final ProjectDTO expectedProject = projectEndpoint.listProject(new ProjectListRequest(token)).getList().get(0);
        @NotNull final ProjectDTO actualProject = projectEndpoint.findByNameProject(new ProjectFindByNameRequest(token, expectedProject.getName())).getProject();
        Assert.assertEquals(expectedProject.getId(), actualProject.getId());
        Assert.assertEquals(expectedProject.getName(), actualProject.getName());
        Assert.assertEquals(expectedProject.getCreated(), actualProject.getCreated());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void removeByIdProjectTest() {
        int size = projectEndpoint.listProject(new ProjectListRequest(token)).getList().size();
        @NotNull final ProjectDTO expectedProject = projectEndpoint.listProject(new ProjectListRequest(token)).getList().get(0);
        projectEndpoint.removeByIdProject(new ProjectRemoveByIdRequest(token, expectedProject.getId()));
        Assert.assertEquals(size - 1, projectEndpoint.listProject(new ProjectListRequest(token)).getList().size());
        Assert.assertFalse(projectEndpoint.listProject(new ProjectListRequest(token)).getList().contains(expectedProject));
    }

    @Category(IntegrationCategory.class)
    @Test
    public void removeByNameProjectTest() {
        int size = projectEndpoint.listProject(new ProjectListRequest(token)).getList().size();
        @NotNull final ProjectDTO expectedProject = projectEndpoint.listProject(new ProjectListRequest(token)).getList().get(0);
        projectEndpoint.removeByNameProject(new ProjectRemoveByNameRequest(token, expectedProject.getName()));
        Assert.assertEquals(size - 1, projectEndpoint.listProject(new ProjectListRequest(token)).getList().size());
        Assert.assertFalse(projectEndpoint.listProject(new ProjectListRequest(token)).getList().contains(expectedProject));
    }

    @Category(IntegrationCategory.class)
    @Test
    public void updateByIdProjectTest() {
        @NotNull final ProjectDTO expectedProject = projectEndpoint.listProject(new ProjectListRequest(token)).getList().get(0);
        projectEndpoint.updateByIdProject(new ProjectUpdateByIdRequest(token, expectedProject.getId(), "newName", "newDescr"));
        @NotNull final ProjectDTO actualProject = projectEndpoint.findByIdProject(new ProjectFindByIdRequest(token, expectedProject.getId())).getProject();
        Assert.assertEquals("newName", actualProject.getName());
        Assert.assertEquals("newDescr", actualProject.getDescription());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void updateByNameProjectTest() {
        @NotNull final ProjectDTO expectedProject = projectEndpoint.listProject(new ProjectListRequest(token)).getList().get(0);
        projectEndpoint.updateByNameProject(new ProjectUpdateByNameRequest(token, expectedProject.getName(), "newName", "newDescr"));
        @NotNull final ProjectDTO actualProject = projectEndpoint.findByIdProject(new ProjectFindByIdRequest(token, expectedProject.getId())).getProject();
        Assert.assertEquals("newName", actualProject.getName());
        Assert.assertEquals("newDescr", actualProject.getDescription());
    }

}
