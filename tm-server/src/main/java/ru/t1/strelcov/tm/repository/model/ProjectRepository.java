package ru.t1.strelcov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.repository.model.IProjectRepository;
import ru.t1.strelcov.tm.model.Project;

import javax.persistence.EntityManager;

public final class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    public Class<Project> getClazz() {
        return Project.class;
    }

}
